=========================================
Pythagorean Theorem Without Words
=========================================

:tags: math, art-maybe, proofs
:slug: pythagorean-sans-verbis
:summary: Pythagorean Theorem Without Words
:date: 03-26-2020
:author: Joseph McKinsey
:modified: 03-26-2020 22:10

Introduction
============

The pythagorean theorem lends itself easily to a wide variety of visual proofs. Try and figure out the logic behind them.
I find it to be clearer in some cases than others.

This is partly inspired by Neal Stephenson's "Anathema" as well as numerous other visual proofs online.
There's a lot of really good ones, but I think there's some room for improvement.

The Pythagorean Theorem
=======================

Attempt 1
---------
.. raw:: html
    :file: pythagoreanTheorem.svg

Attempt 2
---------
.. raw:: html
    :file: pythagoreanTheorem2.svg
    
Attempt 3 (based on Euclid)
---------------------------
.. raw:: html
    :file: pythagoreanTheorem3.svg


Bonus (Perpendicular Bisector Theorem)
======================================
I don't remember why I made this, but I suspect it has something to do with Fortune's algorithm.

.. raw:: html
    :file: perpendicularbisector.svg

